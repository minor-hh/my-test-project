import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: '/about'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to,from,next) => {
  router.addRoutes([
    {
      path: '/test',
      component: () => import("@/views/test.vue")
    },
    {
      path: '/main',
      component: () => import("@/views/main.vue")
    },
  ])
  console.log(to)
  if(to.path === '/main') {
    next({ ...to, replace: true });
  } else {
    next()
  }

})

export default router